package formulas;

import java.util.ArrayList;

public class Dispersion 
{
	public static double calcular(ArrayList<Integer> coeficientes)
	{
		double promedio = calcularPromedio(coeficientes);
		double sumatoria = coeficientes
				.stream()
				.map(d -> Math.pow((d-promedio), 2))
				.reduce(0.0, (a,b) -> a+b);
		return sumatoria/(coeficientes.size()-1);
	}
	
	private static double calcularPromedio(ArrayList<Integer> coeficientes)
	{
		int sumatoria = coeficientes
				.stream()
				.reduce(0 , (a, b) ->  a+b);
		return (sumatoria/((coeficientes.size())*1.0));
	}
}
