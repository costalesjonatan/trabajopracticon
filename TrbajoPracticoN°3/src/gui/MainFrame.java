package gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

import controlllers.MainController;

public class MainFrame 
{

	private JFrame _frame;
	private JButton _agregarEquipo;
	private JButton _generarConfiguracionDeEquipos;
	private JTextPane _vistaEquipos;
	private JTextPane _vistaGrupo1;
	private JTextPane _vistaGrupo2;
	private JTextPane _vistaGrupo3;
	private MainController _controller;

	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try 
				{
					MainFrame window = new MainFrame();
					window._frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public MainFrame() 
	{
		initialize();
		inicializarBotones();
		inicializarTextos();
		_controller = new MainController();
	}
	
	private void initialize() 
	{
		_frame = new JFrame();
		_frame.setBounds(100, 100, 600, 600);
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_frame.getContentPane().setLayout(null);
	}
	
	private void inicializarBotones()
	{
		inicializarAgregarEquipo();
		inicializarGenerarConfiguracionDeEquipos();
	}
	
	private void inicializarTextos()
	{
		inicializarVistaDeEquipos();
		inicializarVistaGrupos();
	}
	
	private void inicializarAgregarEquipo()
	{
		_agregarEquipo = new JButton("Agregar Equipo");
		_agregarEquipo.setBounds(400, 50, 150, 20);
		_agregarEquipo.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				String equipo = JOptionPane.showInputDialog("Ingrese el nombre del equipo");
				if(equipo != null)
				{
					String coeficiente = JOptionPane.showInputDialog("Ingrese el coeficiente del equipo");
					if(coeficiente != null)
					{
						try 
						{
							_controller.agregarEquipos(equipo, Integer.parseInt(coeficiente));
							actualizarTextoEquipos();
						}catch(RuntimeException ex)
						{
							JOptionPane.showMessageDialog(_frame, "A ingresado un coeficiente no valido");
							
						}
					}	
				}
				if(_controller.getCantidadDeEquipos() == 12)
				{
					_agregarEquipo.setVisible(false);
					_generarConfiguracionDeEquipos.setVisible(true);
				}
			}
		});
		_frame.add(_agregarEquipo);
	}
	
	private void inicializarGenerarConfiguracionDeEquipos()
	{
		_generarConfiguracionDeEquipos = new JButton("Generar configuracion");
		_generarConfiguracionDeEquipos.setBounds(400, 50, 150, 20);
		_generarConfiguracionDeEquipos.addActionListener(new ActionListener() 
		{	
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				_controller.generarConfiguracion();
				actualizarTextoGrupos();
			}
		});
		_frame.add(_generarConfiguracionDeEquipos);
		_generarConfiguracionDeEquipos.setVisible(false);
	}
	
	private void inicializarVistaDeEquipos() 
	{
		_vistaEquipos = new JTextPane();
		_vistaEquipos.setBounds(50, 50, 250, 200);
		_vistaEquipos.setEditable(false);
		_vistaEquipos.setBackground(Color.LIGHT_GRAY);
		_frame.add(_vistaEquipos);
	}
	
	private void inicializarVistaGrupos()
	{
		_vistaGrupo1 = new JTextPane();
		_vistaGrupo1.setBounds(0, 350, 200, 100);
		_vistaGrupo1.setBackground(Color.LIGHT_GRAY);
		_vistaGrupo1.setText("Grupo 1");
		_frame.add(_vistaGrupo1);
		_vistaGrupo2 = new JTextPane();
		_vistaGrupo2.setBounds(200, 350, 200, 100);
		_vistaGrupo2.setBackground(Color.LIGHT_GRAY);
		_vistaGrupo2.setText("Grupo 2");
		_frame.add(_vistaGrupo2);
		_vistaGrupo3 = new JTextPane();
		_vistaGrupo3.setBounds(400, 350, 200, 100);
		_vistaGrupo3.setBackground(Color.LIGHT_GRAY);
		_vistaGrupo3.setText("Grupo 3");
		_frame.add(_vistaGrupo3);
	}
	
	private void actualizarTextoEquipos()
	{	
		_vistaEquipos.setText(_controller.getTextoEquipos());
	}
	
	private void actualizarTextoGrupos()
	{
		_vistaGrupo1.setText("Grupo 1\n" + _controller.getTextoGrupo(0).toString());
		_vistaGrupo2.setText("Grupo 2\n" + _controller.getTextoGrupo(1).toString());
		_vistaGrupo3.setText("Grupo 3\n" + _controller.getTextoGrupo(2).toString());
	}

}
