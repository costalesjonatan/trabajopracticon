package models;

import java.util.ArrayList;

public class Grupo 
{
	private ArrayList<Integer> _equipos;
	private int _tamañoMaximo;
	
	public Grupo(int tamañoMaximo)
	{
		_equipos = new ArrayList<Integer>();
		_tamañoMaximo = tamañoMaximo;
	}
	
	public void agergarEquipo(Integer equipo)
	{
		verificarTamañoMax();
		_equipos.add(equipo);
	}
	
	public void quitarEquipo(Integer equipo)
	{
		_equipos.remove(equipo);
	}
	
	private void verificarTamañoMax() 
	{
		if(getTamano() == _tamañoMaximo)
		{
			throw new IllegalArgumentException("El grupo esta lleno");
		}
	}
	
	public int getTamano()
	{
		return _equipos.size();
	}
	
	public Integer getEquipo(int posicion) 
	{
		return _equipos.get(posicion);
	}
	
	public String toString()
	{	
		return _equipos.toString();
	}

	//Un grupo sera igual a otro si y solo si tienen los equipos en las mismas posiciones;
	public boolean equals(Object o)
	{
		if(o instanceof Grupo)
		{
			if(getTamano() == ((Grupo)o).getTamano()) 
			{
				for(int i = 0; i < getTamano(); i++)
				{
					if(!getEquipo(i).equals(((Grupo)o).getEquipo(i)))
						return false;
				}
				return true;
			}
			else 
				return false;
		}
		else 
			return false;
	}

	public boolean contains(Integer equipo) {
		
		for(Integer e: getEquipos())
		{
			if(e.equals(equipo))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public Grupo clonar()
	{
		Grupo clon = new Grupo(_tamañoMaximo);
		
		for(Integer i: getEquipos())
		{
			clon.agergarEquipo(i);
		}
		
		return clon;
	}
	
	public ArrayList<Integer> getEquipos()
	{
		return _equipos;
	}
}
