package models;

import java.util.ArrayList;

public class Configuracion 
{
private ArrayList<Grupo> _grupos;
	
	public Configuracion()
	{
		_grupos = new ArrayList<Grupo>();
	}
	
	public void agregarGrupo(Grupo grupo)
	{
		verificarGrupo(grupo);
		_grupos.add(grupo);
	}
	
	private void verificarGrupo(Grupo grupo)
	{
		if(grupo.getTamano() < 1 || grupo.getTamano() > 4)
		{
			throw new IllegalArgumentException("El grupo debe tener exactamente 4 equipos");
		}
	}
	
	public Grupo getGrupo(int posicion)
	{
		verificarPosicion(posicion);
		return _grupos.get(posicion);
	}
	
	public int getTamao()
	{
		return _grupos.size();
	}
	
	private void verificarPosicion(int posicion)
	{
		if(posicion < 0 || posicion > 2)
		{
			throw new IllegalArgumentException("se solicito el grupo de una posicion no valida; Posicion: " + posicion);
		}
	}

	public void quitarGrupo(Grupo grupo)
	{
		_grupos.remove(grupo);
	}

	public boolean contiene(Grupo grupo) 
	{
		
		for(Grupo g: this._grupos)
		{
			if(g.equals(grupo))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof Configuracion)
		{
			if(getTamao() == ((Configuracion)o).getTamao())
			{
				for(int i = 0; i < getTamao(); i++)
				{
					if(!getGrupo(i).equals(((Configuracion) o).getGrupo(i)))
						return false;
				}
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}

	public Configuracion clonar() 
	{
		Configuracion clon = new Configuracion();
		
		for(Grupo g: this._grupos)
		{
			clon.agregarGrupo(g);
		}

		return clon;
	}

	public boolean noContienEquiposRepetidos(Grupo grupo) 
	{
		for(int i = 0; i < getTamao(); i++)
		{
			for(int j = 0; j < getGrupo(i).getTamano(); j++)
			{
				if(getGrupo(i).contains(grupo.getEquipo(j)))
				{
					return false;
				}
			}
		}
		return true;
	}
	
	public String toString()
	{
		return getGrupos().toString();
	}
	
	public ArrayList<Grupo> getGrupos()
	{
		return _grupos;
	}

}
