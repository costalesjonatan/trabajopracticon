package models;

public class Equipo 
{
	private String _nombre;
	private int _puntos;
	
	public Equipo(String nombre, int puntos)
	{
		verificarArgumentos(nombre, puntos);
		_nombre = nombre;
		_puntos = puntos;
	}
	
	private void verificarArgumentos(String nombre, int fuerza)
	{
		if(nombre == null)
		{
			throw new IllegalArgumentException("El equipo debe tener un nombre"); 
		}
		
		if(fuerza <= 0)
		{
			throw new IllegalArgumentException("La fuerza del equipo debe ser mayor a 0");
		}
	}
	
	public int getPuntos()
	{
		return _puntos;
	}
	
	public String getNombre() 
	{
		return _nombre;
	}
	
	public boolean equals(Object o)
	{
		if(o instanceof Equipo)
		{
			if((getNombre().equals(((Equipo) o).getNombre()) && getPuntos() == ((Equipo) o)._puntos))
			{
				return true;
			}
			else
				return false;
		}
		else {
			return false;
		}
	}
	
	public String toString()
	{
		return "Equipo: " + _nombre + ", Puntos: " + _puntos;
	}

	
}
