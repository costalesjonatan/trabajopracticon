package logic;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import models.Grupo;

public class Combinaciones 
{
	private static ArrayList<Grupo> _grupos = new ArrayList<Grupo>();
	private static ArrayList<Integer> _equipos = new ArrayList<Integer>();
	
	public static ArrayList<Grupo> generarSinRepeticion(int cardinal, int cardinalSubConjunto)
	{
		_grupos.removeAll(_grupos);
		_equipos = (ArrayList<Integer>) Stream.iterate(0, x -> x+1).limit(cardinal).collect(Collectors.toList());
		recursion(new Grupo(cardinalSubConjunto), _equipos, 0, cardinalSubConjunto);
		return _grupos;
	}
	
	private static  void recursion(Grupo grupo ,ArrayList<Integer> equipos, int indice, int cardinalSubConjunto)
	{
		if(grupo.getTamano() == cardinalSubConjunto)
		{
			_grupos.add((Grupo) grupo.clonar());
			return;
		}
		
		for(int i = indice; i < equipos.size(); i++)
		{
			if(!grupo.contains(equipos.get(i))) 
			{
				grupo.agergarEquipo(equipos.get(i));
				recursion(grupo, equipos, i, cardinalSubConjunto);
				grupo.quitarEquipo(equipos.get(i));;
			}
		}
	}
}
