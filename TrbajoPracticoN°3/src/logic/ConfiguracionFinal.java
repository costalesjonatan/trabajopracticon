package logic;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Collectors;
import formulas.Dispersion;
import models.Configuracion;
import models.Equipo;
import models.Grupo;

public class ConfiguracionFinal 
{
	private static ArrayList<Configuracion> _configuraciones = new ArrayList<Configuracion>();
	private static ArrayList<Grupo> _grupos;
	private static ArrayList<Equipo> _equipos;
	
	
	public static ArrayList<ArrayList<String>> obtener(ArrayList<Equipo> equipos, int limiteDeGrupo)
	{
		verificarCantidadDeEquipos(equipos);
		_configuraciones.removeAll(_configuraciones);
		_grupos = Combinaciones.generarSinRepeticion(equipos.size(), limiteDeGrupo);
		_equipos = equipos;
		generar();
		return traducir(obtenerDispersionMayor(obtenerDispersiones()));
	}


	private static void verificarCantidadDeEquipos(ArrayList<Equipo> equipos) {
		if(equipos.size()%2 == 1)
		{
			throw new IllegalArgumentException("El numero de equipos tiene que ser Par");
		}
	}
	
	
	private static void generar()
	{
		recursion(new Configuracion(), 0);
	}

	private static void recursion(Configuracion configuracion, int indice)
	{
		if(configuracion.getTamao() == 3)
		{
			_configuraciones.add(configuracion.clonar());
			return;
		}
		
		for(int i = indice; i < _grupos.size(); i++)
		{
			if(!configuracion.contiene(_grupos.get(i)))
			{
				if(configuracion.noContienEquiposRepetidos(_grupos.get(i)))
				{
					configuracion.agregarGrupo(_grupos.get(i));
					recursion(configuracion, i);
					configuracion.quitarGrupo(_grupos.get(i));
				}
			}
		}	
	}
	
	private static ArrayList<Double> obtenerDispersiones()
	{
		return (ArrayList<Double>) _configuraciones
				.stream()
				.map(c -> c.getGrupos()
				.stream()
				.map(g -> Dispersion.calcular(obtenerCoeficientes(g)))
				.min(new Comparator<Double>() { public int compare(Double arg0, Double arg1) { return (int) (arg0 - arg1);}})
				.get())
				.collect(Collectors
				.toList());
	}
	
	
	private static ArrayList<Integer> obtenerCoeficientes(Grupo grupo) 
	{
		return (ArrayList<Integer>) grupo
				.getEquipos()
				.stream()
				.map(e -> _equipos.get(e)
				.getPuntos())
				.collect(Collectors
				.toList());
	}
	
	private static int obtenerDispersionMayor(ArrayList<Double> dispersiones)
	{	
		return dispersiones
			    .indexOf(dispersiones
			    .stream()
			    .max(new Comparator<Double>(){ public int compare(Double o1, Double o2) { return (int) (o1 - o2); }})
			    .get());
	}
	
	

	private static ArrayList<ArrayList<String>> traducir(int posicion) 
	{
		ArrayList<ArrayList<String>> traduccion = new ArrayList<ArrayList<String>>();
		ArrayList<Grupo> aTraducir = _configuraciones.get(posicion).getGrupos();
		
		for(int i = 0; i < 3; i++)
		{			
			traduccion.add((ArrayList<String>)aTraducir.get(i)
					.getEquipos()
					.stream()
					.map(e -> _equipos.get(e).getNombre())
					.collect(Collectors.toList()));
		}
		return traduccion;
	}
}
