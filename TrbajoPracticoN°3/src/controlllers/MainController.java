package controlllers;

import java.util.ArrayList;

import logic.ConfiguracionFinal;
import models.Equipo;

public class MainController 
{
	
	private ArrayList<Equipo> _equipos;
	private ArrayList<ArrayList<String>> _configuracionFinal;
	
	public MainController()
	{
		_equipos = new ArrayList<Equipo>();
	}
	
	public void agregarEquipos(String nombre, int coeficiente)
	{
		_equipos.add(new Equipo(nombre, coeficiente));
	}
	
	public void generarConfiguracion()
	{
		_configuracionFinal = ConfiguracionFinal.obtener(_equipos, 4);
	}
	
	public String getTextoEquipos()
	{
		StringBuilder ret = new StringBuilder();
		for(Equipo e: _equipos)
		{
			ret.append(e.toString() + "\n");
		}
		return ret.toString();
	}
	
	public String getTextoGrupo(int posicion)
	{
		StringBuilder ret = new StringBuilder();
		ArrayList<String> grupo = _configuracionFinal.get(posicion);
		
		for(String s: grupo)
		{
			ret.append(s + "\n");
		}
		
		return ret.toString();
	}
	
	public int getCantidadDeEquipos()
	{
		return getEquipos().size();
	}
	
	public ArrayList<Equipo> getEquipos()
	{
		return _equipos;
	}
}
