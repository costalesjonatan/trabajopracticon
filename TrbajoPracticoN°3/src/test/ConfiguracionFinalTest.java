package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import logic.ConfiguracionFinal;
import models.Equipo;

class ConfiguracionFinalTest 
{
	private ArrayList<Equipo> _equipos;
	
	@BeforeEach
	public void setUp()
	{
		_equipos = new ArrayList<Equipo>();
		_equipos.add(new Equipo("Paises Bajos", 50));
		_equipos.add(new Equipo("Venezuela", 100));
		_equipos.add(new Equipo("Brasil", 100));
		_equipos.add(new Equipo("Colombia", 10));
		_equipos.add(new Equipo("Japon", 10));
		_equipos.add(new Equipo("Chile", 10));
		_equipos.add(new Equipo("Uruguay", 25));
		_equipos.add(new Equipo("Paraguay", 25));
		_equipos.add(new Equipo("Peru", 25));
		_equipos.add(new Equipo("Ecuador", 50));
		_equipos.add(new Equipo("Mexico", 50));
		_equipos.add(new Equipo("Argentina", 100));
	}
	
	
	@Test
	public void obtenerTest()
	{
		assertEquals(ConfiguracionFinal.obtener(_equipos, 4).toString(), "[[Paises Bajos, Venezuela, Colombia, Uruguay], [Brasil, Japon, Paraguay, Ecuador], [Chile, Peru, Mexico, Argentina]]");
	}

}
