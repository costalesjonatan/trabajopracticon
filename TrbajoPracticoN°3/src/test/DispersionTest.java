package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import formulas.Dispersion;

class DispersionTest 
{
	private ArrayList<Integer> coeficientes;
	
	@BeforeEach
	public void setUp()
	{
		coeficientes = new ArrayList<Integer>();
	}
	
	@AfterEach
	public void aftersetUp()
	{
		coeficientes.removeAll(coeficientes);
	}
	
	@Test
	public void dispersion0test() 
	{
		crearCaso1();
		assertEquals(Dispersion.calcular(coeficientes), 0.0);
	}

	private void crearCaso1() 
	{
		coeficientes.add(1000);
		coeficientes.add(1000);
		coeficientes.add(1000);
		coeficientes.add(1000);
	}
	
	@Test
	public void dispersionNormalTest() 
	{
		crearCaso2();
		assertEquals(Dispersion.calcular(coeficientes), 16.333333333333332);
	}

	private void crearCaso2() 
	{
		coeficientes.add(10);
		coeficientes.add(5);
		coeficientes.add(2);
		coeficientes.add(1);
	}
	
	@Test
	public void dispersionAltaTest() 
	{
		crearCaso3();
		assertEquals(Dispersion.calcular(coeficientes), 249500.25);
	}

	private void crearCaso3() 
	{
		coeficientes.add(1000);
		coeficientes.add(1);
		coeficientes.add(1);
		coeficientes.add(1);
	}
	

}
