package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import controlllers.MainController;
import models.Equipo;

class MainControllerTest 
{
	private MainController _controller;
	
	@BeforeEach
	public void setUp()
	{
		_controller = new MainController();
	}
	
	@Test
	public void getEquiposTest() 
	{
		assertEquals(_controller.getEquipos().getClass(),new ArrayList<Equipo>().getClass());
	}
	
	@Test
	public void agregarEquipoTest()
	{
		_controller.agregarEquipos("Argentina", 100);
		assertTrue(_controller.getEquipos().size() == 1);
	}
	
	@Test
	public void getTextoEquiposTest()
	{
		_controller.agregarEquipos("Argentina", 100);
		assertEquals(_controller.getTextoEquipos(), "Equipo: Argentina, Puntos: 100\n");
	}
	
	@Test
	public void getCantidadEquiposTets()
	{
		assertTrue(_controller.getCantidadDeEquipos() == 0);
	}
	
	@Test
	public void generarConfiguracionGrupo1Test()
	{
		agregarEquipos();
		_controller.generarConfiguracion();
		assertEquals(_controller.getTextoGrupo(0), "Paises Bajos\nBrasil\nColombia\nJapon\n");
	}
	
	@Test
	public void generarConfiguracionGrupo2Test()
	{
		agregarEquipos();
		_controller.generarConfiguracion();
		assertEquals(_controller.getTextoGrupo(1), "Venezuela\nUruguay\nParaguay\nArgentina\n");
	}
	
	@Test
	public void generarConfiguracionGrupo3Test()
	{
		agregarEquipos();
		_controller.generarConfiguracion();
		assertEquals(_controller.getTextoGrupo(2), "Chile\nPeru\nEcuador\nMexico\n");
	}
	
	private void agregarEquipos()
	{
		_controller.agregarEquipos("Paises Bajos", 1554);
		_controller.agregarEquipos("Venezuela", 1484);
		_controller.agregarEquipos("Brasil", 1676);
		_controller.agregarEquipos("Colombia", 1573);
		_controller.agregarEquipos("Japon", 1494);
		_controller.agregarEquipos("Chile", 1559);
		_controller.agregarEquipos("Uruguay", 1613);
		_controller.agregarEquipos("Paraguay", 1467);
		_controller.agregarEquipos("Peru", 1516);
		_controller.agregarEquipos("Ecuador", 1378);
		_controller.agregarEquipos("Mexico", 1549);
		_controller.agregarEquipos("Argentina", 1580);
	}
}
