package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import models.Equipo;

class EquipoTest {

	private Equipo _equipo;
	
	@Test
	public void verificarNombreTest() 
	{
		assertThrows(IllegalArgumentException.class, () -> _equipo= new Equipo(null, 15));
	}
	
	@Test
	public void verificarPuntosTest() 
	{
		assertThrows(IllegalArgumentException.class, () -> _equipo= new Equipo("Argentina", 0));
	}
	
	@Test
	public void verificarToString()
	{
		_equipo = new Equipo("Argentina", 1500);
		assertEquals(_equipo.toString(), "Equipo: Argentina, Puntos: 1500" );
	}
	
	@Test
	public void equalsTest()
	{
		_equipo = new Equipo("Argentina", 1500);
		Equipo equipo2 = new Equipo("Argentina", 1500);
		assertTrue(_equipo.equals(equipo2));
	}
	
	@Test
	public void notEqualsTest()
	{
		_equipo = new Equipo("Argentina", 1500);
		Equipo equipo2 = new Equipo("Venezuela", 600);
		assertFalse(_equipo.equals(equipo2));
	}
	
	@Test
	public void notEqualsNombreTest()
	{
		_equipo = new Equipo("Argentina", 1500);
		Equipo equipo2 = new Equipo("Venezuela", 1500);
		assertFalse(_equipo.equals(equipo2));
	}
	
	@Test
	public void notEqualsPuntosTest()
	{
		_equipo = new Equipo("Argentina", 1500);
		Equipo equipo2 = new Equipo("Argentina", 1400);
		assertFalse(_equipo.equals(equipo2));
	}
	
	@Test
	public void differentClassEqualsTest()
	{
		_equipo = new Equipo("Argentina", 1500);
		assertFalse(_equipo.equals(new Object()));
	}
}
