package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import models.Grupo;

class GrupoTest 
{
	private Grupo _grupo;
	
	@BeforeEach
	public void setup()
	{
		_grupo = new Grupo(4);
		_grupo.agergarEquipo(0);
		_grupo.agergarEquipo(1);
		_grupo.agergarEquipo(2);
	}
	
	@Test
	public void agregarEquipoTest() 
	{
		_grupo.agergarEquipo(4);
		assertTrue(_grupo.getTamano() == 4);
	}
	
	@Test 
	public void verificarTamañoTest()
	{
		_grupo.agergarEquipo(4);
		assertThrows(IllegalArgumentException.class, () -> _grupo.agergarEquipo(5));
	}
	
	@Test
	public void quitarEquipoTest()
	{
		_grupo.quitarEquipo(2);
		assertTrue(_grupo.getTamano() == 2);
	}
	
	@Test
	public void tamañoTest()
	{
		assertTrue(_grupo.getTamano() == 3);
	}
	
	@Test
	public void getEquipoTest()
	{
		assertTrue(_grupo.getEquipo(0) == 0);
	}
	
	@Test
	public void getEquiposTest()
	{
		ArrayList<Integer> esperado = new ArrayList<Integer>();
		esperado.add(0);
		esperado.add(1);
		esperado.add(2);
		assertEquals(_grupo.getEquipos(), esperado);
	}
	
	@Test
	public void equalsTest()
	{
		Grupo otro = new Grupo(4);
		otro.agergarEquipo(0);
		otro.agergarEquipo(1);
		otro.agergarEquipo(2);
		assertEquals(_grupo, otro);
	}
	
	@Test 
	public void noIgualesPorTamañoTest()
	{
		Grupo otro = new Grupo(4);
		assertFalse(_grupo.equals(otro));
	}
	
	@Test 
	public void noIgualesPorEquiposTest()
	{
		Grupo otro = new Grupo(4);
		otro.agergarEquipo(0);
		otro.agergarEquipo(1);
		otro.agergarEquipo(3);
		assertFalse(_grupo.equals(otro));
	}
	
	@Test 
	public void noIgualesPorPermutacionTest()
	{
		Grupo otro = new Grupo(4);
		otro.agergarEquipo(2);
		otro.agergarEquipo(1);
		otro.agergarEquipo(0);
		assertFalse(_grupo.equals(otro));
	}
	
	@Test 
	public void noIgualesPorClaseTest()
	{
		assertFalse(_grupo.equals(new Object()));
	}
	
	@Test
	public void clonarTest()
	{
		Grupo clon = _grupo.clonar();
		assertEquals(_grupo, clon);
	}
	
	@Test
	public void containsTest()
	{
		assertTrue(_grupo.contains(0));
	}
	
	@Test
	public void notContainsTest()
	{
		assertFalse(_grupo.contains(5));
	}
	
	@Test
	public void toStringTest()
	{
		assertEquals(_grupo.toString(), "[0, 1, 2]");
	}
}
