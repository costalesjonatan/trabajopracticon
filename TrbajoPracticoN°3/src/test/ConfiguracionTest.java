package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import models.Configuracion;
import models.Grupo;

class ConfiguracionTest 
{
	private Configuracion _configuracion;
	private Grupo _grupo1;
	
	@BeforeEach
	public void setUp()
	{
		_configuracion = new Configuracion();
	}
	
	@Test
	public void verificarTamañoTest()
	{
		assertTrue(_configuracion.getTamao() == 0);
	}

	@Test
	public void verificarGrupoTest() 
	{
		_grupo1 = new Grupo(4);
		assertThrows(IllegalArgumentException.class, () -> _configuracion.agregarGrupo(_grupo1));
	}
	
	@Test
	public void agregarGrupoTest()
	{
		_configuracion.agregarGrupo(crearGrupo(0, 1, 2, 3));
		assertTrue(_configuracion.getTamao() == 1);
	}
	
	@Test
	public void verificarPosicionNegativaTest()
	{
		assertThrows(IllegalArgumentException.class, () -> _configuracion.getGrupo(-1));
	}
	
	@Test
	public void verificarPosicionTest()
	{
		assertThrows(IllegalArgumentException.class, () -> _configuracion.getGrupo(3));
	}
	
	@Test
	public void getGrupoTest()
	{
		_grupo1 = crearGrupo(0, 1, 2, 3);
		_configuracion.agregarGrupo(_grupo1);
		Grupo esperado = _grupo1.clonar();
		assertEquals(_configuracion.getGrupo(0), esperado);
	}
	
	@Test
	public void quitarGrupoTest()
	{
		_grupo1 = crearGrupo(0, 1, 2, 3);
		_configuracion.agregarGrupo(_grupo1);
		_configuracion.quitarGrupo(_grupo1);
		assertTrue(_configuracion.getTamao() == 0);
	}
	
	@Test
	public void contieneTest()
	{
		_grupo1 = crearGrupo(0, 1, 2, 3);
		_configuracion.agregarGrupo(_grupo1);
		assertTrue(_configuracion.contiene(_grupo1));
	}
	
	@Test
	public void noContienTest()
	{
		_grupo1 = crearGrupo(0, 1, 2, 3);
		assertFalse(_configuracion.contiene(_grupo1));
	}
	
	@Test
	public void notEqualsTest()
	{
		_configuracion = crearConfiguracion(crearGrupo(0,1,2,3), crearGrupo(4,5,6,7), crearGrupo(11,8,9,10));
		Configuracion otro = crearConfiguracion(crearGrupo(0, 1, 2, 3), crearGrupo(4, 5, 6, 7), crearGrupo(8, 9, 10, 11));
		assertFalse(_configuracion.equals(otro));
	}
	
	@Test
	public void notEqualsPorTamañoTest()
	{
		_configuracion = crearConfiguracion(crearGrupo(0,1,2,3), crearGrupo(4,5,6,7), crearGrupo(8,9,10,11));
		_configuracion.quitarGrupo(_configuracion.getGrupo(2));
		Configuracion otro = crearConfiguracion(crearGrupo(0, 1, 2, 3), crearGrupo(4, 5, 6, 7), crearGrupo(8, 9, 10, 11));
		assertFalse(_configuracion.equals(otro));
	}
	
	@Test
	public void notEqualsPorClaseTest()
	{
		assertFalse(_configuracion.equals(new Object()));
	}
	
	@Test
	public void equalsTest()
	{
		_configuracion = crearConfiguracion(crearGrupo(0,1,2,3), crearGrupo(4,5,6,7), crearGrupo(8,9,10,11));
		Configuracion otro = crearConfiguracion(crearGrupo(0, 1, 2, 3), crearGrupo(4, 5, 6, 7), crearGrupo(8, 9, 10, 11));
		assertEquals(_configuracion, otro);
	}
	
	@Test
	public void clonarTest()
	{
		_configuracion = crearConfiguracion(crearGrupo(0,1,2,3), crearGrupo(4,5,6,7), crearGrupo(8,9,10,11));
		Configuracion clon = _configuracion.clonar();
		assertEquals(_configuracion, clon);
	}
	
	@Test
	public void notContieneEquiposRepetidosTest()
	{
		_configuracion.agregarGrupo(crearGrupo(0,1,2,3));
		assertTrue(_configuracion.noContienEquiposRepetidos(crearGrupo(4,5,6,7)));
	}
	
	@Test
	public void siContieneEquiposRepetidosTest()
	{
		_configuracion.agregarGrupo(crearGrupo(0,1,2,3));
		assertFalse(_configuracion.noContienEquiposRepetidos(crearGrupo(4,5,6,3)));
	}
	
	@Test
	public void toStringTest()
	{
		_configuracion = crearConfiguracion(crearGrupo(0,1,2,3), crearGrupo(4,5,6,7), crearGrupo(8,9,10,11));
		assertEquals(_configuracion.toString(), "[[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11]]");
	}
	
	@Test
	public void getGruposTest()
	{
		_configuracion = crearConfiguracion(crearGrupo(0,1,2,3), crearGrupo(4,5,6,7), crearGrupo(8,9,10,11));
		ArrayList<Grupo> esperado = crear();
		assertEquals(_configuracion.getGrupos(), esperado);
	}
	
	private Grupo crearGrupo(int equipo1, int equipo2, int equipo3, int equipo4)
	{
		Grupo ret = new Grupo(4);
		ret.agergarEquipo(equipo1);
		ret.agergarEquipo(equipo2);
		ret.agergarEquipo(equipo3);
		ret.agergarEquipo(equipo4);
		return ret;
	}
	
	private Configuracion crearConfiguracion(Grupo grupo1, Grupo grupo2, Grupo grupo3) 
	{
		Configuracion ret = new Configuracion();
		ret.agregarGrupo(grupo1);
		ret.agregarGrupo(grupo2);
		ret.agregarGrupo(grupo3);
		return ret;
	}
	
	private ArrayList<Grupo> crear() 
	{
		ArrayList<Grupo> esperado = new ArrayList<Grupo>();
		int k = 0;
		for(int i = 0; i < 3; i++)
		{
			esperado.add(crearGrupo(k, k+1, k+2, k+3));
			k+=4;
		}
		return esperado;
	}
}
