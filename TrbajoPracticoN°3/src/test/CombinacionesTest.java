package test;

import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import logic.Combinaciones;
import models.Grupo;

class CombinacionesTest 
{
	
	private ArrayList<Grupo> _combinaciones = Combinaciones.generarSinRepeticion(4, 3);
	
	@Test
	public void generarTest() 
	{
		assertEquals(_combinaciones.size(), 4);
	}
}
